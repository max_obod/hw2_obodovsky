print("Введите свой возвраст: ")
age = input()
age = int(age)

if age <= 0:
    print("Невозможно.")
elif age % 10 == 0:
    print("Где сертификат от вакцинации?")
elif age < 7:
    print("Где твои мама и папа?")
elif age < 18:
    print("Мы не продаем сигареты несовершеннолетним!")
elif age > 65:
    print("Вы в зоне риска!")
else:
    print("Оденьте маску!")